fonts-nanum-eco (1.000-8) UNRELEASED; urgency=medium

  * Remove 1 obsolete maintscript entry.
  * Bump debhelper from old 12 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 23 Oct 2022 22:40:22 -0000

fonts-nanum-eco (1.000-7) unstable; urgency=medium

  * debian/rules:
    - Do not override dh_builddeb and switch back to the default. The
      default compression is now xz and good enough.
  * debian/control:
    - Update Vcs-* to use salsa.debian.org
    - Update Maintainer to use the new debian-fonts list
    - Bump Standards-Version to 4.4.0
    - Update upstream homepage
  * debian/{compat,control}:
    - Use debhelper 12
  * debian/copyright:
    - Use https for the copyright format URI
    - Update upstream homepage
  * d/salsa-ci.yml: Add salsa CI config

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 23 Jul 2019 00:17:23 -0300

fonts-nanum-eco (1.000-6) unstable; urgency=medium

  * Switch to -8e xz compression level for binary package
    - For this package, result size of -9e is the same as -8e, while
      requiring 32 MiB additional space for decompression.
  * Update and correct the copyright format

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 21 Sep 2014 02:56:47 +0900

fonts-nanum-eco (1.000-5) unstable; urgency=medium

  [ Changwoo Ryu ]
  * Move font configs to /usr/share/fontconfig/conf.avail/

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 29 Jun 2014 12:33:38 +0900

fonts-nanum-eco (1.000-4) unstable; urgency=medium

  [ Changwoo Ryu ]
  * debian/90-fonts-nanum-eco.conf
    - Correct ExtraBold weight and style
    - Add default font specific hinting
  * Change the install path to .../nanum/ from .../nanum-eco/ .
  * Standards-Version: 3.9.5

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 22 Apr 2014 06:47:07 +0900

fonts-nanum-eco (1.000-3) unstable; urgency=low

  [ Changwoo Ryu ]
  * Correct fontconfig conf for recent fontconfig (Closes: #715258)
  * Build-Depends on dpkg >= 1.16.2 for dpkg-deb -Sextreme option
  * Standards-Version: 3.9.4
  * Add Multi-Arch: foreign
  * Add comment-only debian/watch file

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 03 Aug 2013 17:13:27 +0900

fonts-nanum-eco (1.000-2) unstable; urgency=low

  [ Changwoo Ryu ]
  * Move to git
  * Use xz to compress the binary packages
  * Use debhelper 9
  * Standards-Version: 3.9.3
  * Use the copyright format 1.0

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 03 Jun 2012 02:25:13 +0900

fonts-nanum-eco (1.000-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * Initial version (Closes: #646952)

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 05 Nov 2011 23:26:13 +0900
